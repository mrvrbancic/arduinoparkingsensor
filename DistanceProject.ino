#define ULTRASONIC_PIN 4
#define DISTANCE_MIN 0
#define DISTANCE_MAX 330
#define DELAY 500

#define BLUE 8
#define RED 10
#define GREEN 9

#define PIR_PIN 7
#define BUZZ_PIN 11

#include <Servo.h>
Servo servo;

void setup()
{
  Serial.begin(9600);
  pinMode(ULTRASONIC_PIN, INPUT);
  pinMode(PIR_PIN, INPUT);
  pinMode(BLUE, OUTPUT);
  pinMode(RED, OUTPUT);
  pinMode(GREEN, OUTPUT);
}

void loop()
{
  //Reasons, dons't work like it should
  /*while(!digitalRead(PIR_PIN))
  {
    Serial.print(digitalRead(PIR_PIN));
    digitalWrite(BLUE,LOW);
    digitalWrite(GREEN,LOW);
    digitalWrite(RED,LOW);
    noTone(BUZZ_PIN);  
  };*/
  // calculate data
  int distance = get_distance(ULTRASONIC_PIN);
  
  // print data
 // Serial.print("distance: ");
 // Serial.print(distance);

  activate_devices(distance);
  
  // apply data

  delay(DELAY);
}

int get_distance(int pin) 
{
  // clear the trigger
  pinMode(pin, OUTPUT);
  digitalWrite(pin, LOW);
  delayMicroseconds(2);
  
  // set the triger pin on HIGH state for 10 micro seconds
  digitalWrite(pin, HIGH);
  delayMicroseconds(10);
  digitalWrite(pin, LOW);

  // read echo pin: returns the sound wave travel time in microseconds
  pinMode(pin, INPUT);
  unsigned long duration = pulseIn(pin, HIGH);
  
  // calculate the distance: s = t * v / 2
  return duration * 0.0343 / 2;
}

void activate_devices(float distance) 
{
  if( distance < 20)
  {
    digitalWrite(RED,HIGH);
    digitalWrite(GREEN,LOW);
    digitalWrite(BLUE,LOW);
    tone(BUZZ_PIN, 500);
  }
  else if(distance > 20 && distance < 50)
  {
    digitalWrite(RED,LOW);
    digitalWrite(GREEN,HIGH);
    digitalWrite(BLUE,LOW);
    tone(BUZZ_PIN, 500, 200);
  }
  else
  {
    digitalWrite(RED,LOW);
    digitalWrite(GREEN,LOW);
    digitalWrite(BLUE,HIGH);
    noTone(BUZZ_PIN);
  }
}

